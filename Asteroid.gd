extends RigidBody2D
class_name Asteroid

@onready var outer_shape = $Line2D
@onready var fill_shape = $Polygon2D
@onready var collision = $CollisionPolygon2D

@export var points = PackedVector2Array()
@export var texture : Texture2D

var texture_size
var starter_rect

var min_fragment_size = 300

var area = 0;
var pre_centered = false

signal projectile_hit
var widest = 0

static func spawn(world, params = null):
	var instance = Main.asteroid_scene.instantiate()
	if params:
		instance.position = params.position 
		instance.rotation = params.rotation
		instance.linear_velocity = params.linear_velocity
		instance.angular_velocity = params.angular_velocity
		instance.points = params.points
		instance.area = params.area
	else: 
		instance.points = Shapes.draw_random_rock(50, 70)
		instance.angular_velocity = randf_range(-1, 1)
	world.add_child(instance)
	instance.add_to_group('asteroids')
	return instance


func _ready():
	outer_shape.points = points
	fill_shape.polygon = points
	if area == 0: area = Shapes.calculate_polygon_properties(points).area
	mass = set_asteroid_mass()
	collision.set_deferred("polygon", points)
	get_widest()
	connect("projectile_hit", on_projectile_hit)

func set_asteroid_mass():
	return area / 100

func get_widest():
	var rect = Shapes.calculate_bounding_rect(points)
	widest = max(rect.size.x, rect.size.y)

func set_colors(col_1, col_2):
	outer_shape.default_color = col_1
	fill_shape.color = col_2
	
func change_shape(polygon: PackedVector2Array):
	var poly = Shapes.calculate_polygon_properties(polygon)
	if poly.area >= 0 and poly.area <= min_fragment_size:
		queue_free()
		return
	area = poly.area
	mass = set_asteroid_mass()
	collision.set_deferred("polygon", polygon)
	outer_shape.points = polygon
	fill_shape.polygon = polygon
	center_of_mass = poly.center

func spawn_piece(polygon: PackedVector2Array, hit_direction):
	if polygon.size() <= 2: 
		return
	var polygon_properties = Shapes.calculate_polygon_properties(polygon)
	var new_area = polygon_properties.area
	if new_area >= 0 and new_area <= min_fragment_size: return
	var center_mass = polygon_properties.center
	var centered = PackedVector2Array(Shapes.translate_points(polygon, center_mass))
	var new_position = global_position + transform.basis_xform(center_mass)
	var new_rect
	if starter_rect != null:
		new_rect = starter_rect
		new_rect.position -= transform.basis_xform(center_mass)
	
	Asteroid.spawn(get_parent(), {
		"position": new_position,
		"rotation": rotation,
		"linear_velocity": linear_velocity + hit_direction,
		"angular_velocity": angular_velocity,
		"points": centered,
		"area": new_area
	})

func on_projectile_hit(hit_point: Vector2, hit_direction: Vector2):
	var transformed_hit_point = transform.basis_xform_inv(hit_point - global_position)
	var hole_shape
	if randf() > 0.0: # adjust the odds of a shatter vs a regular hole
		hole_shape = Shapes.draw_random_shatter(15, 30, widest*1.1, 2, transformed_hit_point)
	else: 
		hole_shape = Shapes.draw_random_convex(20, 30, 2, transformed_hit_point)
	var polygons = Geometry2D.clip_polygons(outer_shape.points, hole_shape)
	var impact_polygons = Geometry2D.intersect_polygons(outer_shape.points, hole_shape)
	if impact_polygons.size() == 0:
		return
	var impact_shape = impact_polygons[0]
	if polygons.size() == 0:
		queue_free()
		return
	elif polygons.size() == 1:
		if polygons[0].size() <= 2:
			queue_free()
			return
		change_shape(polygons[0])
	elif polygons.size() > 1:
		for i in range(1, polygons.size()):
			spawn_piece(polygons[i], Vector2.ZERO)
		change_shape(polygons[0])
		
	apply_impulse(hit_direction, hit_point-global_position)
	return true
