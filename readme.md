# Godot 4.2 asteroid destruction physics demo

A Godot 4.2 project demonstrating the Geometry2D class to implement destructable asteroids

A working demo game utilising this can be found here:
https://dhdev10.itch.io/alpha-star-pilot

video of the above demo:
https://x.com/i/status/1783284537335677003

Any issues just ask I guess.

### Todo:
Add more comments, and write a better readme