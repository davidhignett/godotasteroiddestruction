extends RigidBody2D

func _ready():
	body_entered.connect(on_body_entered)
	$Timer.timeout.connect(die)
	
func on_body_entered(body):
	if body is Asteroid:
		body.on_projectile_hit(global_position, linear_velocity.normalized()*150)
		die()

func die():
	queue_free()
