extends Node2D
class_name Main

static var projectile_scene = preload("res://Projectile.tscn")
static var asteroid_scene = preload("res://Asteroid.tscn")
var world_mouse_position = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	Asteroid.spawn(self)

func _physics_process(delta):
	var viewport = get_viewport()
	var view_rect = viewport.get_visible_rect()
	world_mouse_position = viewport.get_mouse_position() - (view_rect.size/2)
	
	if Input.is_action_just_pressed('click'):
		var instance = projectile_scene.instantiate()
		instance.position = world_mouse_position
		add_child(instance)


func _on_reset():
	for asteroid in get_tree().get_nodes_in_group('asteroids'):
		asteroid.queue_free()
	Asteroid.spawn(self)
