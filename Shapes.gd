class_name Shapes

static func create_point_around_center(angle: float, distance: float) -> Vector2:
	var point = Vector2(sin(angle), cos(angle));
	return point * distance

static func draw_random_convex(min_radius: float, max_radius: float, resolution: float, offset: Vector2 = Vector2.ZERO): 
	var steps = ceil(((min_radius + max_radius) / 2.0) / resolution)
	var points = []
	for i in steps:
		var angle = (PI*2)/steps * i;
		var radius = randf_range(min_radius, max_radius)
		points.append(offset + create_point_around_center(angle, radius));
	return points
	
static func rotate_vector(v, radians):
	var s = sin(radians);
	var c = cos(radians);
	var tx = v.x;
	var ty = v.y;
	return Vector2(c * tx - s * ty, s * tx + c * ty);
	
static func translate_points(points, offset, angle = 0):
	var translated = [] 
	for p in points:
		var t = rotate_vector(p, angle) - offset
		translated.append(t)
	return translated
	
static func calculate_bounding_rect(points) -> Rect2:
	var smallest = Vector2.ZERO
	var largest = Vector2.ZERO
	for point in points:
		if point.x > largest.x: largest.x = point.x
		if point.x < smallest.x: smallest.x = point.x
		if point.y > largest.y: largest.y = point.y
		if point.y < smallest.y: smallest.y = point.y
	var size = -smallest + largest
	var position = (smallest + largest) /2
	return Rect2(position, size)
	
static func calculate_polygon_properties(points):
	var center = Vector2.ZERO
	var area = 0
	for i in points.size():
		var next = 0 if i + 1 == points.size() else i + 1
		var p = points[i]
		var q = points[next]
		area += (p.x * q.y) - (q.x * p.y)
		center.x += (pow(p.x, 2) + p.x * q.x + pow(q.x, 2)) * (q.y - p.y)
		center.y -= (pow(p.y, 2) + p.y * q.y + pow(q.y, 2)) * (q.x - p.x)
	area = abs(area/2)
	center = (center / 6) / area
	return { 
		"center": center, 
		"area": area
	}

static func get_furthest_point(points):
	var furthest = points[0]
	for i in range(1, points.size()):
		if points[i].length() > furthest.length(): furthest = points[i]
	return furthest.length()
	
static func draw_random_shatter(
	min_radius: float,
	max_radius: float,
	spur_length: float,
	resolution: float,
	offset: Vector2 = Vector2.ZERO,
	set_steps: int = 0
): 
	var steps = ceil(((min_radius + max_radius) / 2.0) / resolution) if set_steps == 0 else set_steps
	var points = []
	var sector = (PI*2)/steps
	var previous_sector_was_spur = false
	var first_sector_was_spur = false
	var number_of_spurs = 0
	for i in steps:
		var angle = sector * i
		if  randf() >= 0.5 and !previous_sector_was_spur:
			number_of_spurs += 1
			# prevent neighbouring spurs and potential overlaps causing non-convex shapes
			previous_sector_was_spur = true
			if i == 0:
				# prevent edge case where neighbouring spurs can generate and potentially overlap
				# if the first and last sectors are both spurs
				first_sector_was_spur = true;
			if i == steps - 1 and first_sector_was_spur:
				break
			var spur_start_angle = sector * (i - 1)
			var spur_point_angle = randf_range(spur_start_angle, spur_start_angle + sector)
			var spur_deform_range = sector/4
			var spur_deform_angles = [
				spur_point_angle + randf_range(-spur_deform_range, spur_deform_range),
				spur_point_angle + randf_range(-spur_deform_range, spur_deform_range),
				spur_point_angle + randf_range(-spur_deform_range, spur_deform_range),
				spur_point_angle + randf_range(-spur_deform_range, spur_deform_range),
			]
			# add outward vertex
			var spur_divisions = spur_length / spur_deform_angles.size() + 1
			for j in range(1, spur_deform_angles.size()):
				points.append(offset + create_point_around_center(spur_deform_angles[j], spur_divisions * j+1));
			# add end point vertex
			points.append(offset + create_point_around_center(spur_point_angle, spur_length));
			# add returning vertex
			for j in range(1, spur_deform_angles.size()):
				var k = spur_deform_angles.size() - j
				points.append(offset + create_point_around_center(spur_deform_angles[k] + spur_deform_range/k, spur_divisions * k+1));
		else:
			previous_sector_was_spur = false
		var radius = randf_range(min_radius, max_radius)

		points.append(offset + create_point_around_center(angle, radius));
		
	if number_of_spurs < 3:
		return draw_random_shatter(min_radius, max_radius, spur_length, resolution, offset, steps)
	return points

static func draw_random_rock(maximum: float, minimum: float):
	var clumps = randi_range(1, 4)
	var random_shape_fallback
	var random_shape
	for i in clumps:
		if i == 0:
			random_shape = Shapes.draw_random_convex(minimum, maximum, 6)
			if random_shape.size() <= 4:
				random_shape = Shapes.draw_random_convex(minimum, maximum, 3)
			random_shape_fallback = random_shape
			continue
		var offset = Vector2(randi_range(-1, 1) * minimum, randi_range(-1, 1) * minimum)
		var clump = Shapes.draw_random_convex(minimum, maximum, 6, offset)
		var union = Geometry2D.merge_polygons(random_shape, clump)
		for polygon in union:
			if !Geometry2D.is_polygon_clockwise(polygon):
				random_shape = polygon
	if random_shape.size() <= 3:
		# Sometimes the fancy union operations above would go wrong and we'd end up with an unimpressive shape
		return random_shape_fallback
	else:
		return random_shape

static func create_rock_with_radius(maximum: float):
	var minimum = maximum * 0.75
	var shape = draw_random_rock(maximum, minimum)
	var shape_data = calculate_polygon_properties(shape)
	var centered = translate_points(shape, shape_data.center)
	var radius = get_furthest_point(centered)
	return {
		"radius": radius,
		"shape": centered,
		"area": shape_data.area
	}
